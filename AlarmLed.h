#pragma once

#include "Led.h"
#include "AlarmTimer.h"


class AlarmLed
{
public:
    AlarmLed(int8_t pin, const LedLevels *levels, const AlarmTimer *timer)
        : _led(pin, levels)
        , _timer(timer)
    {}
    
    void set(bool enabled)
    {
        if (enabled)
            _led.set(_timer->state());
        else
            _led.set(LedState::Off);
    }
    
private:
    Led _led;
    const AlarmTimer *const _timer;
};
