#pragma once

#include <Arduino.h>
#include "GyverButton.h"
#include "GyverTimer.h"
#include "Dimmer.h"
#include "Disconnector.h"


enum class ChannelMode
{
    Off,
    Right,
    Left,
};


class OutputChannel
{
public:
    OutputChannel(int8_t pin_enable_power, int8_t pin_enable_led, int8_t pin_side_switch, ChannelMode initial_mode, Dimmer *dimmer)
        : _disconnector(pin_enable_power)
        , _pin_enable_led(pin_enable_led)
        , _pin_side_switch(pin_side_switch)
        , _mode(initial_mode)
        , _dimmer(dimmer)
    {
        pinMode(_pin_enable_led, OUTPUT);
        pinMode(_pin_side_switch, OUTPUT);
        
        _switch.setDebounce(80);
        _switch.setTimeout(500);
        _switch.setClickTimeout(100);
        _switch.setType(LOW_PULL);
        _switch.setDirection(NORM_OPEN);
    }
    
    void set_mode(ChannelMode mode)
    {
        _mode = mode;
    }
    
    void tick(bool pressed)
    {
        _switch.tick(pressed);
        const bool changed = _switch.isClick();
        
        if (changed)
        {
            _dimmer->disable();
            if (_mode >= ChannelMode::Left)
                _mode = ChannelMode::Off;
            else
                _mode = static_cast<ChannelMode>(static_cast<int>(_mode) + 1);
        }
        _disconnector.tick(_mode == ChannelMode::Off, _mode != ChannelMode::Right && changed);

        digitalWrite(_pin_enable_led, _mode == ChannelMode::Off ? HIGH : LOW);
        digitalWrite(_pin_side_switch, _mode == ChannelMode::Left ? HIGH : LOW);
    }
    
    bool enabled() const
    {
        return _mode != ChannelMode::Off;
    }
        
private:
    Disconnector _disconnector;
    const int8_t _pin_enable_led;
    const int8_t _pin_side_switch;
    GButton _switch;
    ChannelMode _mode;
    Dimmer *const _dimmer;
};
