#pragma once

#include <Arduino.h>

enum class Power
{
    Off,
    Insufficient,
    On
};


class PowerSource
{
public:
    PowerSource(int8_t pin, int insufficient, int on)
        : _pin(pin)
        , _insufficient(insufficient)
        , _on(on)
        , _level(Power::Off)
        , _changed(false)
    {
        pinMode(pin, INPUT);
    }
    
    void tick()
    {
        int analog_level = analogRead(_pin);
        Power new_level = Power::Insufficient;
        
        if (analog_level < _insufficient)
            new_level = Power::Off;
        else if (analog_level > _on)
            new_level = Power::On;
            
        _changed = new_level != _level;
        _level = new_level;
    }
    
    Power get_level() const
    {
        return _level;
    }
    
    bool is_changed() const
    {
        return _changed;
    }
    
private:
    const int8_t _pin;
    const int _insufficient;
    const int _on;
    Power _level;
    bool _changed;
};
