#pragma once

#include <Arduino.h>
#include "LedState.h"
#include "LedLevels.h"


class Led
{
public:
    Led(int8_t pin, const LedLevels *levels)
        : _pin(pin)
        , _levels(levels)
    {
        pinMode(pin, OUTPUT);
    }
    
    void set(LedState state)
    {
        analogWrite(_pin, _levels->getValue(state));
    }
    
private:
    const int8_t _pin;
    const LedLevels *const _levels;
};
