#pragma once

#include "Led.h"
#include "AlarmTimer.h"
#include "Dimmer.h"


enum class SideLedMode
{
    Red,
    Green,
    Orange,
    Alarm
};


class SideLed
{
public:
    SideLed(int8_t green_pin, int8_t red_pin, const LedLevels *green_levels, const LedLevels *red_levels, const AlarmTimer *timer, const Dimmer *dimmer)
        : _green(green_pin, green_levels)
        , _red(red_pin, red_levels)
        , _timer(timer)
        , _dimmer(dimmer)
    {}
    
    void set(SideLedMode mode)
    {
        if (mode == SideLedMode::Alarm)
        {
            _green.set(LedState::Off);
            _red.set(_timer->state());
        }
        else
        {
            LedState bright = _dimmer->getLedState();
            _green.set(mode != SideLedMode::Red ? bright : LedState::Off);
            _red.set(mode != SideLedMode::Green ? bright : LedState::Off);
        }
    }
    
private:
    Led _green;
    Led _red;
    const AlarmTimer *const _timer;
    const Dimmer *const _dimmer;
};
