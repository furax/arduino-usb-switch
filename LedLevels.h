#pragma once

#include "LedState.h"


class LedLevels
{
public:
    LedLevels(uint8_t bright, uint8_t dimmed)
        : _bright(255 - bright)
        , _dimmed(255 - dimmed)
        , _off(255)
    {}
    
    uint8_t getValue(LedState state) const
    {
        return _values[static_cast<int>(state)];
    }
    
private:
    union
    {
        struct
        {
            const uint8_t _bright;
            const uint8_t _dimmed;
            const uint8_t _off;
        };
        uint8_t _values[3];
    };
};
