#pragma once

#include <Arduino.h>
#include "GyverTimer.h"


class Disconnector
{
public:
    Disconnector(int8_t pin)
        : _pin(pin)
        , _timer(MS)
        , _disconnecting(false)
        , _permanent(false)
    {
        pinMode(_pin, OUTPUT);

        _timer.setMode(MANUAL);
        _timer.setTimeout(2000);
        _timer.stop();
    }
    
    void tick(bool permanent, bool temporary)
    {
        if (_timer.isReady())
            _disconnecting = false;
        
        if (temporary)
        {
            if (!_disconnecting && !_permanent)
            {
                _disconnecting = true;
                _timer.start();
            }
        }
        
        _permanent = permanent;
        digitalWrite(_pin, _disconnecting || _permanent ? HIGH : LOW);
    }
        
private:
    const int8_t _pin;
    GTimer _timer;
    bool _disconnecting;
    bool _permanent;
};
