#pragma once


enum class LedState
{
    Bright,
    Dimmed,
    Off
};

