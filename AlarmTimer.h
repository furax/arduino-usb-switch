#pragma once

#include <Arduino.h>
#include "LedState.h"
#include "GyverTimer.h"


class AlarmTimer
{
public:
    AlarmTimer(uint32_t dimmed_ms, uint32_t bright_ms)
        : _dimmed_ms(dimmed_ms)
        , _bright_ms(bright_ms)
        , _timer(MS)
        , _state(LedState::Dimmed)
    {
        _timer.setMode(MANUAL);
        _timer.setTimeout(dimmed_ms);
    }
    
    void tick()
    {
        if (_timer.isReady())
        {
            if (_state == LedState::Dimmed)
            {
                _state = LedState::Bright;
                _timer.setTimeout(_bright_ms);
            }
            else
            {
                _state = LedState::Dimmed;
                _timer.setTimeout(_dimmed_ms);
            }
            _timer.start();
        }
    }
    
    LedState state() const
    {
        return _state;
    }
    
private:
    const uint32_t _dimmed_ms;
    const uint32_t _bright_ms;
    GTimer _timer;
    LedState _state;
};
