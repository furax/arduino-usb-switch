#pragma once

#include "LedState.h"
#include "GyverTimer.h"


class Dimmer
{
public:
    Dimmer()
        : _dimmed(false), _timer(MS)
    {
        _timer.setMode(MANUAL);
        _timer.setTimeout(5000);
    }
    
    void tick()
    {
        if (_timer.isReady())
        {
            _dimmed = true;
        }
    }
    
    void disable()
    {
        _dimmed = false;
        _timer.start();
    }
    
    LedState getLedState() const
    {
        return _dimmed ? LedState::Dimmed : LedState::Bright;
    }
    
private:
    bool _dimmed;
    GTimer _timer;
};
