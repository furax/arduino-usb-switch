#include <Arduino.h>
#include "Dimmer.h"
#include "OutputChannel.h"
#include "PowerSource.h"
#include "LedLevels.h"
#include "AlarmTimer.h"
#include "SideLed.h"
#include "AlarmLed.h"


Dimmer dimmer;

PowerSource dock_station(A1, 500, 500);
PowerSource phone(A2, 500, 500);
PowerSource desktop(A6, 171, 597);

OutputChannel keyboard_and_mouse(A4, A5, 13, ChannelMode::Right, &dimmer);
OutputChannel sound_card(4, 7, 8, ChannelMode::Right, &dimmer);
OutputChannel camera(2, 12, A3, ChannelMode::Off, &dimmer);

LedLevels channel_red(255, 96);
LedLevels channel_green(255, 96);
LedLevels alarm_red(255, 32);

AlarmTimer channel_alarm(750, 750);
AlarmTimer alarm_alarm(4500, 500);

SideLed left(9, 6, &channel_green, &channel_red, &channel_alarm, &dimmer);
SideLed right(3, 5, &channel_green, &channel_red, &channel_alarm, &dimmer);

AlarmLed sound_card_alarm(10, &alarm_red, &alarm_alarm);
AlarmLed camera_alarm(11, &alarm_red, &alarm_alarm);

Disconnector left_disconnector(A0);

void setup()
{
    pinMode(A7, INPUT);
    
    if (phone.get_level() == Power::On && dock_station.get_level() == Power::Off && desktop.get_level() == Power::Off)
    {
        keyboard_and_mouse.set_mode(ChannelMode::Off);
        sound_card.set_mode(ChannelMode::Left);
    }
}


void loop()
{
    dimmer.tick();
    
    dock_station.tick();
    phone.tick();
    desktop.tick();
    
    const int button = analogRead(A7);
    int pressed = 0;
    if (button >= 717)
        pressed = 3;
    else if (button >= 307)
        pressed = 2;
    else if (button >= 102)
        pressed = 1;
    
    keyboard_and_mouse.tick(pressed == 1);
    sound_card.tick(pressed == 2);
    camera.tick(pressed == 3);
    
    channel_alarm.tick();
    alarm_alarm.tick();
    
    left_disconnector.tick(false, phone.is_changed());
    
    if (phone.get_level() == Power::On)
        left.set(SideLedMode::Orange);
    else if (dock_station.get_level() == Power::On)
        left.set(SideLedMode::Green);
    else
        left.set(SideLedMode::Red);
    
    if (desktop.get_level() == Power::Insufficient)
        right.set(SideLedMode::Alarm);
    else if (desktop.get_level() == Power::On)
        right.set(SideLedMode::Green);
    else
        right.set(SideLedMode::Red);

    sound_card_alarm.set(sound_card.enabled() && desktop.get_level() != Power::On && (keyboard_and_mouse.enabled() || camera.enabled()));
    camera_alarm.set(camera.enabled());

    delay(10);
}
